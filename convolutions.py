import tensorflow as tf
import numpy as np


def get_variable(name, shape, l2_strength, initialiser=tf.contrib.layers.xavier_initializer()):
    """A convenience method for creating a set of variables with L2 regularisation using the Xavier initialisation
    by default. The method automatically adds the regularisation to tf.GraphKeys.REGULARIZATION_LOSSES.

    :param name: A string. Name of the variable.
    :param shape: A list of ints. Shape of the resulting variable.
    :param l2_strength: A float. The parameter for the strength of the L2 regularisation.
    :param initialiser: An initializer object. The initialisation method to use to create this variable.
    :return:
    """
    return tf.get_variable(name,
                           shape=shape,
                           initializer=initialiser,
                           dtype=tf.float32,
                           regularizer=tf.contrib.layers.l2_regularizer(l2_strength))


def conv_bias_activation(x, kernel_shape, strides, padding, bias, l2_strength, use_batchnorm, activation, is_training,
                         name):
    """Wrapper around Tensorflows conv2d function. Add option batch normalisation and activation function.

    :param x: A tensor of shape [batch, height, width, channels].
    :param kernel_shape: A list of ints of the form [filter_height, filter_width, in_channels, out_channels]
    :param strides: A list of ints. The stride for each dimension of x.
    :param padding: A string of either "SAME" or "VALID". The type of padding to use.
    :param bias: A float. The initial value of the bias term.
    :param l2_strength: A float. Strength of the L2 regularisation.
    :param use_batchnorm: A boolean. To use batch normalisation or not.
    :param activation: A Tensorflow operation. The activation function to apply after the convolution.
    :param is_training: A boolean. For use with batch normalisation to indicate training or testing.
    :param name: A string. The name to set the variable_scope.
    :return: A tensor of the same type as x.
    """

    with tf.variable_scope(name):
        kernel = get_variable("weights", kernel_shape, l2_strength)

        conv = tf.nn.conv2d(x,
                            kernel,
                            strides=strides,
                            padding=padding)

        biases = tf.get_variable(initializer=tf.constant_initializer(bias),
                                 shape=[kernel.shape[-1]],
                                 dtype=tf.float32,
                                 name='biases')

        conv_bias = tf.nn.bias_add(conv, biases)

        if use_batchnorm:
            conv_bias_bn = tf.layers.batch_normalization(conv_bias, training=is_training, name='batch_normalization')
        else:
            conv_bias_bn = conv_bias

        if activation is not None:
            result = activation(conv_bias_bn)
        else:
            result = conv_bias_bn

        return result


def grouped_conv(x,
                 kernel_shape,
                 strides,
                 num_groups,
                 bias,
                 l2_strength,
                 use_batchnorm,
                 activation,
                 is_training,
                 name=None):
    """Applies grouped convolution to the input tensor and optional batch normalisation and activation.

    :param x: A tensor of shape [batch, height, width, channels].
    :param kernel_shape: A list of ints of the form [filter_height, filter_width, in_channels, out_channels]
    :param strides: A list of ints. The stride for each dimension of x.
    :param num_groups: An int. The number of groups to use.
    :param bias: A float. The amount of bias.
    :param l2_strength: A float. Strength of the L2 regularisation.
    :param use_batchnorm: A boolean. To use batch normalisation or not.
    :param activation: A Tensorflow operation. The activation function to apply after the convolution.
    :param is_training: A boolean. For use with batch normalisation to indicate training or testing.
    :param name: A string. The name to set the variable_scope.
    :return: A tensor of the same type as x.
    """

    group_size = x.shape[3].value // num_groups
    kernel_shape[2] = kernel_shape[2] // num_groups
    kernel_shape[3] = kernel_shape[3] // num_groups

    with tf.variable_scope(name):
        convs = []
        for i in range(num_groups):
            conv = conv_bias_activation(x=x[:, :, :, i * group_size: (i + 1) * group_size], kernel_shape=kernel_shape,
                                        strides=strides, padding='VALID', bias=bias, l2_strength=l2_strength,
                                        use_batchnorm=False, activation=None, is_training=is_training,
                                        name=name + '_' + str(i))

            convs.append(conv)

        group_conv = tf.concat(convs, axis=-1)

        if use_batchnorm:
            group_conv_bn = tf.layers.batch_normalization(
                group_conv, training=is_training, name='batch_normalization')
        else:
            group_conv_bn = group_conv

        if activation is not None:
            result = activation(group_conv_bn)
        else:
            result = group_conv_bn

    return result


def depthwise_conv(x, kernel_shape, strides, bias, l2_strength, use_batchnorm, activation, is_training, name):
    """Applies depthwise convolution to the input tensor x and add optional batch normalisation and activation.

    :param x: A tensor of shape [batch, height, width, channels].
    :param kernel_shape: A list of ints of the form [filter_height, filter_width, in_channels, out_channels]
    :param strides: A list of ints. The stride for each dimension of x.
    :param bias: A float. The amount of bias.
    :param l2_strength: A float. Strength of the L2 regularisation.
    :param use_batchnorm: A boolean. To use batch normalisation or not.
    :param activation: A Tensorflow operation. The activation function to apply after the convolution.
    :param is_training: A boolean. For use with batch normalisation to indicate training or testing.
    :param name: A string. The name to set the variable_scope.
    :return: A tensor of the same type as x.
    """

    with tf.variable_scope(name):
        kernel = get_variable("weights", kernel_shape, l2_strength)

        conv = tf.nn.depthwise_conv2d(x, kernel, strides, 'VALID')

        biases = tf.get_variable(initializer=tf.constant_initializer(bias),
                                 shape=[x.shape[-1]],
                                 dtype=tf.float32,
                                 name='biases')

        conv_bias = tf.nn.bias_add(conv, biases)

        if use_batchnorm:
            conv_bias_bn = tf.layers.batch_normalization(conv_bias, training=is_training, epsilon=1e-5, name='batch_normalization')
        else:
            conv_bias_bn = conv_bias

        if activation is not None:
            result = activation(conv_bias_bn)
        else:
            result = conv_bias_bn

        return result


def _resolve_shape(tensor, rank=None):
    """Fully resolves the shape of a tensor.

    :param tensor: A tensor whose shape we wish to query.
    :param rank: An int. The rank of the tensor if known.
    :return: tf.shape. Shape of the tensor.
    """

    with tf.name_scope("resolve_shape", values=[tensor]):
        if rank is not None:
            shape = tensor.get_shape().with_rank(rank).as_list()
        else:
            shape = tensor.get_shape().as_list()

        if None in shape:
            shape_dynamic = tf.shape(tensor)
            for i in range(len(shape)):
                if shape[i] is None:
                    shape[i] = shape_dynamic[i]

    return shape


def shuffle_channels(x, num_groups, name):
    """Shuffles the channels of the input tensor x.

    :param x: A tensor of shape [batch, height, width, channels].
    :param num_groups: An int. The number of groups to split the channels into before shuffling
    :param name: A string. The name to set the variable_scope.
    :return: A tensor of the same type as x.
    """
    with tf.variable_scope(name):
        n, h, w, c = _resolve_shape(x)
        x_reshaped = tf.reshape(x, [n, h, w, num_groups, c // num_groups])
        x_transposed = tf.transpose(x_reshaped, [0, 1, 2, 4, 3])
        output = tf.reshape(x_transposed, [n, h, w, c])

    return output


def upsample(x, upsample_factor, output_shape, l2_strength, bias, use_batchnorm, is_training, name):
    """Upsamples an image tensor using transposed convolution with optional batch normalisation.

    :param x: A tensor of shape [batch, height, width, channels].
    :param upsample_factor: An int. The upsample scale factor.
    :param output_shape: A list of ints of the form [output_height, output_width, out_channels]
    :param l2_strength: A float. Strength of the L2 regularisation.
    :param bias: A float. The amount of bias.
    :param use_batchnorm: A boolean. To use batch normalisation or not.
    :param is_training: A boolean. For use with batch normalisation to indicate training or testing.
    :param name: A string. The name to set the variable_scope.
    :return: A tensor of the same type as x.
    """
    kernel_size = 2 * upsample_factor - upsample_factor % 2
    kernel_shape = [kernel_size, kernel_size, output_shape[-1], x.shape[-1]]

    output_shape = [x.shape[0]] + output_shape

    strides = [1, upsample_factor, upsample_factor, 1]

    return conv_tranpose(x, kernel_shape, output_shape, strides, l2_strength, bias, use_batchnorm, is_training, name)


def conv_tranpose(x, kernel_shape, output_shape, strides, l2_strength, bias, use_batchnorm, is_training, name):
    """Performs transposed convolution and optionally applies batch normalisation.

    :param x: A tensor of shape [batch, height, width, channels].
    :param kernel_shape: A list of ints of the form [filter_height, filter_width, out_channels, in_channels]
    :param output_shape: A list of ints of the form [batch, output_height, output_width, out_channels]
    :param strides: A list of ints. The stride for each dimension of x.
    :param l2_strength: A float. Strength of the L2 regularisation.
    :param bias: A float. The amount of bias.
    :param use_batchnorm: A boolean. To use batch normalisation or not.
    :param is_training: A boolean. For use with batch normalisation to indicate training or testing.
    :param name: A string. The name to set the variable_scope.
    :return: A tensor of the same type as x.
    """

    with tf.variable_scope(name):
        upsample_factor = strides[1]

        kernel = bilinear_weights(kernel_shape, upsample_factor, l2_strength)

        output_shape_tensor = tf.stack(output_shape)

        conv = tf.nn.conv2d_transpose(x,
                                      filter=kernel,
                                      output_shape=output_shape_tensor,
                                      strides=strides)

        biases = tf.get_variable('biases', [output_shape[-1]], initializer=tf.constant_initializer(bias))

        conv_bias = tf.nn.bias_add(conv, biases)

        if use_batchnorm:
            conv_bias_bn = tf.layers.batch_normalization(conv_bias, training=is_training, name='batch_normalization')
        else:
            conv_bias_bn = conv_bias

        return conv_bias_bn


def bilinear_weights(kernel_shape, upsample_factor, l2_strength):
    """Create a bilinear kernel for transposed convolution for upsampling an image tensor.

    :param kernel_shape: List of ints. Shape of the kernel to create.
    :param upsample_factor: An int. The upsample scale factor.
    :param l2_strength: A float. Strength of the L2 regularisation.
    :return: The created variable.
    """
    if kernel_shape[0] % 2 == 1:
        center_pos = upsample_factor - 1
    else:
        center_pos = upsample_factor - 0.5

    values = np.zeros([kernel_shape[0], kernel_shape[1]], dtype=np.float32)
    for x in range(kernel_shape[1]):
        for y in range(kernel_shape[0]):
            values[x, y] = (1 - abs((center_pos - x) / upsample_factor)) * (1 - abs((center_pos - y) / upsample_factor))

    weights_init = np.zeros(kernel_shape, dtype=np.float32)

    for depth_index in range(kernel_shape[2]):
        weights_init[:, :, depth_index, depth_index] = values

    weights = get_variable("weights", kernel_shape, l2_strength, tf.constant_initializer(weights_init))

    return weights
