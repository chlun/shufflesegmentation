import tensorflow as tf
import os
from tensorflow.python.tools import freeze_graph
from models.shuffleseg import ShuffleSeg
from utils.config import process_config
from utils.utils import get_args


def export_graph(config):
    """Exports the model and the trained weights from the specified checkpoint to a .pb file.

    :param config: Configurations.
    """
    with tf.Graph().as_default():
        input_image = tf.placeholder(tf.uint8,
                                     [1, None, None, config.num_channels],
                                     name=config.input_tensor_name)

        input_image_float = tf.to_float(input_image)

        model = ShuffleSeg(config, image_tensor=input_image_float, mask_tensor=None)
        model.build_model()

        checkpoint = config.checkpoint_file
        saver = tf.train.Saver()

        with tf.Session() as sess:
            saver.restore(sess, checkpoint)
            predictions = tf.identity(model.predictions, name=config.output_tensor_name)

        tf.gfile.MakeDirs(os.path.dirname("./"))

        freeze_graph.freeze_graph_with_def_protos(
            tf.get_default_graph().as_graph_def(add_shapes=True),
            saver.as_saver_def(),
            checkpoint,
            config.output_tensor_name,
            restore_op_name=None,
            filename_tensor_name=None,
            output_graph=config.output_graph_name,
            clear_devices=True,
            initializer_nodes=None)


def main():
    try:
        args = get_args()
        print("Loading config file {0}".format(args.config))
        config = process_config(args.config)
    except Exception as ex:
        print("Missing or invalid arguments: {0}".format(ex.args))
        exit(0)

    export_graph(config)


if __name__ == '__main__':
    main()
