import tensorflow as tf
import os
from tqdm import tqdm
import numpy as np


class BaseTrain:
    def __init__(self, sess, model, data, config, logger):
        """Initialises the trainer.

        :param sess: A Tensorflow session.
        :param model: A subclass of BaseModel.
        :param data: A subclass of BaseData.
        :param config: The configurations.
        :param logger: A Logger object. Logs various statistics and metrics of the training.
        """
        self.model = model
        self.logger = logger
        self.config = config
        self.sess = sess
        self.data = data
        self.train_handle = None
        self.val_handle = None

        self.model.load(sess)

    def train(self):
        """Runs the training for the number of epochs specified in the config. Validation is run on the validation set
        after a specified number of epochs. Checkpoint saving and logging is performed at the specified frequency and
        at the end of the training.

        """
        self.train_handle = self.sess.run(self.data.train_iterator.string_handle())
        self.val_handle = self.sess.run(self.data.val_iterator.string_handle())

        for cur_epoch in range(self.model.cur_epoch_tensor.eval(self.sess), self.config.num_epochs, 1):
            self.train_epoch()
            self.sess.run(self.model.increment_cur_epoch_tensor)

            if (cur_epoch + 1) % self.config.validation_frequency == 0:
                cur_step = self.model.global_step_tensor.eval(self.sess)
                self.validate(cur_step)

        self.model.save(self.sess)

    def train_epoch(self):
        """Runs training for an epoch.
        """
        raise NotImplementedError

    def train_step(self):
        """Does a single training step within an epoch.
        """
        raise NotImplementedError

    def validate(self, step):
        """Runs the model on the validation dataset once.

        :param step: An int. The number of steps training has ran for.
        """
        raise NotImplementedError
