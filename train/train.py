# -*- coding: utf-8 -*-

from train.base_train import BaseTrain
from tqdm import tqdm
import numpy as np
import datetime


class Train(BaseTrain):
    def __init__(self, sess, model, data, config, logger, metrics):
        super(Train, self).__init__(sess, model, data, config, logger)
        self.metrics = metrics

    def train_epoch(self):
        train_data_size = self.data.dataset_size
        num_batches_per_epoch = train_data_size // self.config.batch_size
        loop = tqdm(range(num_batches_per_epoch))

        losses = []
        accs = []
        
        for _ in loop:
            loss, accuracy = self.train_step()
            losses.append(loss)
            accs.append(accuracy)

            cur_step = self.model.global_step_tensor.eval(self.sess)

            if cur_step % self.config.summarise_frequency == 0:
                loss = np.mean(losses)
                accuracy = np.mean(accs)
                summaries_dict = {'loss' : loss, 'accuracy' : accuracy}
                self.logger.summarise(cur_step, summaries_dict=summaries_dict)

            if cur_step % self.config.checkpoint_frequency == 0:
                self.model.save(self.sess)

    def train_step(self):
        feed_dict = {
            self.data.handle: self.train_handle,
            self.model.is_training: True
        }

        _, step, loss, accuracy = self.sess.run(
                [self.model.train_op, self.model.global_step_tensor, self.model.loss, self.model.accuracy],
                feed_dict=feed_dict)

        time_str = datetime.datetime.now().isoformat()
        cur_epoch = self.model.cur_epoch_tensor.eval(self.sess)
        print("{}: epoch {}, step {}, loss {:g}, accuracy {:g}"
              .format(time_str, cur_epoch, step, loss, accuracy))
        
        return loss, accuracy

    def validate(self, step):
        print("Running validation after {} steps".format(step))

        num_batches_per_epoch = self.data.val_dataset_size // self.config.batch_size
        loop = tqdm(range(num_batches_per_epoch))

        self.metrics.reset()
        accs = []

        for _ in loop:
            feed_dict = {
                self.data.handle: self.val_handle,
                self.model.is_training: False
            }

            mask_batch, accuracy, predictions = \
                self.sess.run([self.model.y, self.model.accuracy, self.model.predictions], feed_dict=feed_dict)

            accs.append(accuracy)
            self.metrics.update_stats(np.squeeze(mask_batch, axis=-1).astype(np.int32), predictions)

        mean_metric = self.metrics.compute_mean_metric()
        accuracy = np.mean(accs)

        summary_dict = {"accuracy": accuracy, self.metrics.name: mean_metric}
        self.logger.summarise(step, summariser="val", summaries_dict=summary_dict)

        print("Mean {}: {}".format(self.metrics.name, mean_metric))

        if mean_metric > self.model.current_best_metric.eval(self.sess):
            # Save the new best checkpoint.
            self.model.save_best(self.sess)
            self.sess.run(self.model.update_best_metric(mean_metric))
