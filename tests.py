import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from utils.config import process_config
from data_loader.voc_pascal import VocPascal


def check_checkpoint_variables():
    config = process_config("./configs/shuffleseg_test.json")

    from tensorflow.python.tools.inspect_checkpoint import print_tensors_in_checkpoint_file
    
    latest_ckp = tf.train.latest_checkpoint(config.checkpoint_dir)
    print_tensors_in_checkpoint_file(latest_ckp, all_tensors=True, tensor_name="")


def test_data_aug():
    config = process_config("./configs/shuffleseg_test.json")
    data = VocPascal(config)

    with tf.Session() as sess:
        image, mask = data.get_batch()
        image = tf.squeeze(image, 0)
        mask = tf.squeeze(mask, 0)

        data_handle = sess.run(data.train_iterator.string_handle())

        while True:
            i, m = sess.run([image, mask], feed_dict={data.handle: data_handle})
            plt.imshow(i.astype(np.int32))
            plt.show()
            plt.imshow(np.squeeze(m, axis=2))
            plt.show()


def test_data_handle():
    config = process_config("./configs/shuffleseg_test.json")
    data = VocPascal(config)

    with tf.Session() as sess:
        train_handle = sess.run(data.train_iterator.string_handle())
        val_handle = sess.run(data.val_iterator.string_handle())

        for i in range(10):
            image, mask = sess.run(data.next_elements, feed_dict={data.handle: train_handle})
            plt.imshow(np.squeeze(image, 0).astype(np.int32))
            plt.show()
            plt.imshow(np.squeeze(np.squeeze(mask, 0), 2))
            plt.show()

        for i in range(5):
            image, mask = sess.run(data.next_elements, feed_dict={data.handle: val_handle})
            plt.imshow(np.squeeze(image, 0).astype(np.int32))
            plt.show()
            plt.imshow(np.squeeze(np.squeeze(mask, 0), 2))
            plt.show()


def main():
    #check_checkpoint_variables()
    #test_data_aug()
    test_data_handle()


if __name__ == '__main__':
    main()
