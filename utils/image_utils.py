import tensorflow as tf
import numpy as np


colors = [
    [0, 0, 0],
    [255, 255, 0],
    [0, 255, 255],
    [255, 0, 255],
    [0, 128, 0],
    [128, 0, 0],
    [128, 128, 0],
    [50, 52, 238],
    [0, 0, 255],
    [128, 0, 128],
    [0, 128, 128],
    [0, 0, 128],
    [0, 255, 0],
    [255, 165, 0],
    [0, 100, 0],
    [255, 0, 0],
    [175, 238, 238],
    [75, 0, 130],
    [139, 68, 19],
    [46, 237, 137],
    [193, 50, 195]
]


def apply_mask(image, mask, ignore_classes=[0], alpha=0.5):
    """Apply mask to the image, overlaying each class with a different colour.

    :param image: Array of shape [height, width, 3]. The image to apply the mask to.
    :param mask: Array of ints of shape [height, width]. The mask to apply.
    :param ignore_classes: List of ints. Classes to ignore in the mask.
    :param alpha: A float. Transparency of the overlay.
    :return: Array of the same type as image.
    """
    classes = np.unique(mask)

    for class_ in classes:
        if class_ in ignore_classes:
            continue
        color = colors[class_] if class_ < len(colors) else list(np.random.choice(range(256), size=3))
        for c in range(3):
            image[:, :, c] = np.where(mask == class_,
                                      image[:, :, c] * (1 - alpha) + alpha * color[c],
                                      image[:, :, c])

    return image


def pad_image(image, pad_height, pad_width):
    """Adds padding to an image on the bottom and to the right.

    :param image: The input image.
    :param pad_height: The height to add at the bottom.
    :param pad_width: The width to add to the right.
    :return: The padded image.
    """
    paddings = [[0, pad_height], [0, pad_width], [0, 0]]
    padded_image = tf.pad(image, paddings, "CONSTANT")

    return padded_image


def crop_image_and_label(image, label, offset, cropped_size):
    """Crops the image and label identically.

    :param image: Image of shape [height, width, img_depth]
    :param label: Image of shape [height, width, label_depth]
    :param offset: List of ints. [offset_height, offset_width]. The starting point of the crop.
    :param cropped_size: List of ints. [cropped_height, cropped_width]. The resulting size after cropping.
    :return: The cropped image and label.
    """
    image_depth = tf.shape(image)[2]
    label_depth = tf.shape(label)[2]
    offset_tensor = tf.stack(tf.to_int32([offset[0], offset[1], 0]))

    image_crop_shape = tf.stack([cropped_size[0], cropped_size[1], image_depth])
    label_crop_shape = tf.stack([cropped_size[0], cropped_size[1], label_depth])

    cropped_image = tf.slice(image, offset_tensor, image_crop_shape)
    cropped_label = tf.slice(label, offset_tensor, label_crop_shape)

    return cropped_image, cropped_label


def scale_image_and_label(image, label, target_height, target_width):
    """Scale the image and label identically.

    :param image: Image of shape [height, width, img_depth]
    :param label: Image of shape [height, width, label_depth]
    :param target_height: An int. Height of output.
    :param target_width: An int. Width of output.
    :return: The scaled image and label.
    """
    image = tf.squeeze(tf.image.resize_bicubic(
        tf.expand_dims(image, 0),
        [target_height, target_width],
        align_corners=True), [0])

    label = tf.squeeze(tf.image.resize_nearest_neighbor(
        tf.expand_dims(label, 0),
        [target_height, target_width],
        align_corners=True), [0])

    return image, label


def resize_and_pad_image_and_label(image, label, target_height, target_width):
    """Resize image and label whilst keeping the aspect ratio. If the target aspect ratio doesn't match the current
    one then a padding of zeros is added after resizing.

    :param image: Image of shape [height, width, img_depth]
    :param label: Image of shape [height, width, label_depth]
    :param target_height: An int. Height of output.
    :param target_width: An int. Width of output.
    :return: Resized image and label.
    """
    image = tf.image.resize_image_with_pad(image, target_height, target_width)
    label = tf.image.resize_image_with_pad(label, target_height, target_width)

    return image, label


def randomly_crop_and_resize_image_and_label(image, label, target_height, target_width, max_crop_ratio=0.5):
    """Crop a random area of the image and label and resize to the target dimensions. The crop maintains the aspect
    ratio but not necessarily for resizing which depends on the target dimensions.

    :param image: Image of shape [height, width, img_depth]
    :param label: Image of shape [height, width, label_depth]
    :param target_height: An int. Height of output.
    :param target_width: An int. Width of output.
    :param max_crop_ratio: A float. The largest possible amount as a proportion to reduce each dimension's length
        by cropping.
    :return: The randomly cropped and resized image and label.
    """
    original_size = tf.shape(image)[0:2]

    ratio = tf.random_uniform([], minval=1-max_crop_ratio, maxval=1)
    cropped_size = tf.to_int32(tf.to_float(original_size) * ratio)

    max_offset_height = tf.to_float(original_size[0] - cropped_size[0])
    max_offset_width = tf.to_float(original_size[1] - cropped_size[1])

    offset_height = tf.random_uniform([], minval=0, maxval=max_offset_height)
    offset_width = tf.random_uniform([], minval=0, maxval=max_offset_width)

    cropped_image, cropped_label = crop_image_and_label(image, label, [offset_height, offset_width], cropped_size)

    return scale_image_and_label(cropped_image, cropped_label, target_height, target_width)


def randomly_scale_image_and_label(image, label, min_scale_factor, max_scale_factor):
    """Scales the image and label by a random scale factor whilst maintaining the aspect ratio.

    :param image: Image of shape [height, width, img_depth]
    :param label: Image of shape [height, width, label_depth]
    :param min_scale_factor: A float. Lower bound for the scale factor.
    :param max_scale_factor: A float. Upper bound for the scale factor.
    :return: The resized image and label.
    """
    scale = tf.random_uniform([1], minval=min_scale_factor, maxval=max_scale_factor)
    new_size = tf.to_int32(tf.to_float(tf.shape(image)[0:2]) * scale)

    return scale_image_and_label(image, label, new_size[0], new_size[1])


def randomly_flip_image_and_label_left_right(image, label):
    """Flips image and label horizontally or do nothing with equal probability.

    :param image: Image of shape [height, width, img_depth]
    :param label: Image of shape [height, width, label_depth]
    :return: The flipped image and label or the same image and label unaltered.
    """
    random_number = tf.random_uniform([], minval=0, maxval=1)

    def flip():
        img = tf.image.flip_left_right(image)
        lbl = tf.image.flip_left_right(label)
        return img, lbl

    def no_flip():
        return image, label

    return tf.cond(random_number < 0.5,
                   lambda: flip(),
                   lambda: no_flip())
