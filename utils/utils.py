import argparse
import pickle


def get_args():
    argparser = argparse.ArgumentParser(description=__doc__)
    argparser.add_argument(
        '-c', '--config',
        metavar='C',
        default='None',
        help='The Configuration file')
    args = argparser.parse_args()
    return args


def save_to_pickle(data, path):
    try:
        with open(path, 'wb') as f:
            pickle.dump(data, f)
    except Exception as e:
        print('Unable to save data to', path, ':', e)
        raise
        

def load_pickle(path):
    try:
        with open(path, 'rb') as f:
            return pickle.load(f)
    except Exception as e:
        print("Unable to load data from", path, ":", e)
        raise   