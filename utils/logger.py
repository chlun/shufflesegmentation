import tensorflow as tf
import os


class Logger():
    def __init__(self, sess, config):
        self.sess = sess
        self.config = config
        self.summary_placeholders = {}
        self.summary_ops = {}
        self.train_summary_writer = tf.summary.FileWriter(os.path.join(self.config.summary_dir, "train"), self.sess.graph)
        self.test_summary_writer = tf.summary.FileWriter(os.path.join(self.config.summary_dir, "test"))
        self.val_summary_writer = tf.summary.FileWriter(os.path.join(self.config.summary_dir, "val"))

    def summarise(self, step, summariser="train", scope="", summaries_dict=None):
        """Writes summaries to file using the specified summary writer.

        :param step: An int. The step of the summary.
        :param summariser: A string. Use the train summary writer or the test one.
        :param scope: A string. The name of the variable scope.
        :param summaries_dict: A dict. The dictionary of the summary values (key, value)
        """

        if summariser == "train":
            summary_writer = self.train_summary_writer
        elif summariser == "val":
            summary_writer = self.val_summary_writer
        else:
            summary_writer = self.test_summary_writer

        with tf.variable_scope(scope):

            if summaries_dict is not None:
                summary_list = []
                for key, value in summaries_dict.items():
                    if key not in self.summary_ops:
                        if len(value.shape) <= 1:
                            self.summary_placeholders[key] = tf.placeholder('float32', value.shape, name=key)
                        else:
                            self.summary_placeholders[key] = tf.placeholder('float32', [None] + list(value.shape[1:]), name=key)
                        if len(value.shape) <= 1:
                            self.summary_ops[key] = tf.summary.scalar(key, self.summary_placeholders[key])
                        else:
                            self.summary_ops[key] = tf.summary.image(key, self.summary_placeholders[key])

                    summary_list.append(self.sess.run(self.summary_ops[key], {self.summary_placeholders[key]: value}))

                for summary in summary_list:
                    summary_writer.add_summary(summary, step)
                summary_writer.flush()
