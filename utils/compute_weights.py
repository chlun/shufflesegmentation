import numpy as np
from data_loader.process_voc_pascal import read_from_tfrecords
import os


def get_weights(num_classes, tfrecord_path, ignore_classes=[]):
    examples = read_from_tfrecords(tfrecord_path)

    class_labels = list(range(num_classes + 1))  # +1 to include the void class.

    label_to_frequency= {}
    for example in examples:
        mask = example[1]

        for class_label in class_labels:
            if class_label not in ignore_classes:
                class_mask = np.equal(mask, class_label)
                class_mask = class_mask.astype(np.float32)
            else:
                class_mask = np.zeros(mask.shape, dtype=np.float32)

            class_freq = label_to_frequency.get(class_label)

            if class_freq is None:
                label_to_frequency[class_label] = np.sum(class_mask)
            else:
                label_to_frequency[class_label] = class_freq + np.sum(class_mask)

    class_weights = []
    total_frequency = sum(label_to_frequency.values())

    print("Total frequency : {0}".format(total_frequency))

    for label, frequency in label_to_frequency.items():
        print("Class {0}: Frequency: {1}".format(label, frequency))
        class_weight = 0 if label in ignore_classes else 1 / np.log(1.02 + (frequency / total_frequency))
        class_weights.append(class_weight)

    return class_weights


def main():
    dir_path = "/home/chin/PycharmProjects/ShuffleSeg/data_loader/tfrecords"
    path = os.path.join(dir_path, "voc2012_segmentation_train.tfrecords")
    weights = get_weights(21, path, [1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 13, 14, 16, 17, 18, 19, 20, 21])

    print("Weights:\n")
    print(weights)


if __name__ == '__main__':
    main()
