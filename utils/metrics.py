from sklearn.metrics import confusion_matrix
import numpy as np


class Metrics:
    def __init__(self, config):
        self.config = config

    def update_stats(self, y_true, y_pred):
        """Update with new predictions.

        :param y_true: True labels.
        :param y_pred: Predicted labels.
        """
        raise NotImplementedError

    def compute_mean_metric(self):
        """Compute the average value of the metric so far.

        :return: A float.
        """
        raise NotImplementedError

    def reset(self):
        """Reset metrics and the stored stats to their initial values.
        """
        raise NotImplementedError


class IoU(Metrics):
    def __init__(self, config):
        super(IoU, self).__init__(config)
        self.name = "IoU"
        self.current_iou = 0
        self.conf_mat = np.zeros([self.config.num_classes, self.config.num_classes])

    def update_stats(self, y_true, y_pred):
        for batch_index in range(y_true.shape[0]):
            self.conf_mat = confusion_matrix(y_true[batch_index].flatten(),
                                             y_pred[batch_index].flatten(),
                                             labels=range(self.config.num_classes))

    def compute_mean_metric(self):
        # True positive of each class as an array.
        true_positives = np.diagonal(self.conf_mat)

        unions = np.sum(self.conf_mat, axis=0) + np.sum(self.conf_mat, axis=1) - true_positives

        # IoU of each class.
        ious = true_positives / unions

        # Ignore NaNs caused by some classes having not appeared on been predicted at all.
        self.current_iou = np.nanmean(ious)

        return self.current_iou

    def reset(self):
        self.conf_mat = np.zeros([self.config.num_classes, self.config.num_classes])
        self.current_iou = 0
