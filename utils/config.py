import json
from bunch import Bunch
import os


def get_config_from_json(json_file):
    """Get the configurations from a json file.

    :param json_file: A string. Name of the json file.
    :return: config(namespace) or config(dictionary)
    """
    # Parse the configurations from the config json file provided.
    with open(json_file, 'r') as config_file:
        config_dict = json.load(config_file)

    # Convert the dictionary to a namespace using bunch lib.
    config = Bunch(config_dict)

    return config, config_dict


def process_config(jsonfile):
    """Process the specified json file into a config object.

    :param jsonfile: A string. Name of the json file.
    :return: A config object.
    """
    config, config_dict = get_config_from_json(jsonfile)

    keys = config_dict.keys()

    # Default configs if not specified.
    if "summary_dir" not in keys or config.summary_dir is None:
        config.summary_dir = os.path.join("../experiments", config.exp_name, "summary/")
    if "checkpoint_dir" not in keys or config.checkpoint_dir is None:
        config.checkpoint_dir = os.path.join("../experiments", config.exp_name, "checkpoint/")
    if "best_checkpoint_dir" not in keys or config.best_checkpoint_dir is None:
        config.best_checkpoint_dir = os.path.join("../experiments", config.exp_name, "checkpoint/best")

    if "max_to_keep" not in keys or config.max_to_keep is None:
        config.max_to_keep = 5

    return config
