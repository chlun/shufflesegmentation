import tensorflow as tf
from data_loader.data_loader_factory import DataLoaderFactory
from models.shuffleseg import ShuffleSeg
from train.train import Train
from utils.config import process_config
from utils.logger import Logger
from utils.metrics import IoU
from utils.utils import get_args


def main():
    try:
        args = get_args()
        print("Loading config file {0}".format(args.config))
        config = process_config(args.config)
    except Exception as ex:
        print("Missing or invalid arguments: {0}".format(ex.args))
        exit(0)

    data = DataLoaderFactory.create_loader(config)
    image_tensor, mask_tensor = data.get_batch()
    model = ShuffleSeg(config, image_tensor, mask_tensor, data.get_class_weights())
    model.build_model()
    metrics = IoU(config)

    with tf.Session() as sess:
        logger = Logger(sess, config)
        train = Train(sess, model, data, config, logger, metrics)

        print("Begin training.")
        train.train()


if __name__ == '__main__':
    main()
