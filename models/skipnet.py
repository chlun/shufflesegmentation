from convolutions import *
import tensorflow as tf


class SkipNet:

    def __init__(self, config, is_training, score, feed1, feed2, input_shape):
        # Configs.
        self.config = config
        self.is_training = is_training
        self.input_shape = input_shape

        # Layers
        self.feed1 = feed1
        self.feed2 = feed2
        self.score = score
        self.logits = None

        # Initiate the model.
        self.build_model()

    def build_model(self):
        with tf.variable_scope("SkipNet"):
            feed1_conv = conv_bias_activation(self.feed1,
                                              kernel_shape=[1, 1, self.feed1.shape[-1], self.config.num_classes],
                                              strides=[1, 1, 1, 1],
                                              padding='VALID',
                                              bias=self.config.bias,
                                              l2_strength=self.config.l2_strength,
                                              use_batchnorm=self.config.use_batchnorm,
                                              activation=None,
                                              is_training=self.is_training,
                                              name='feed1_conv1x1')

            print(feed1_conv)

            feed2_conv = conv_bias_activation(self.feed2,
                                              kernel_shape=[1, 1, self.feed2.shape[-1], self.config.num_classes],
                                              strides=[1, 1, 1, 1],
                                              padding='VALID',
                                              bias=self.config.bias,
                                              l2_strength=self.config.l2_strength,
                                              use_batchnorm=self.config.use_batchnorm,
                                              activation=None,
                                              is_training=self.is_training,
                                              name='feed2_conv1x1')

            print(feed2_conv)

            feed2_conv_shape = tf.shape(feed2_conv)

            # Upsample by 2x.
            score_up = upsample(self.score,
                                upsample_factor=2,
                                output_shape=[feed2_conv_shape[1], feed2_conv_shape[2], self.config.num_classes],
                                l2_strength=self.config.l2_strength,
                                bias=self.config.bias,
                                use_batchnorm=self.config.use_batchnorm,
                                is_training=self.is_training,
                                name="score_upsample2x")

            score_feed2 = tf.add(score_up, feed2_conv)

            print(score_feed2)

            feed1_conv_shape = tf.shape(feed1_conv)

            # Upsample by 2x.
            score_feed2_up = upsample(score_feed2,
                                      upsample_factor=2,
                                      output_shape=[feed1_conv_shape[1], feed1_conv_shape[2], self.config.num_classes],
                                      l2_strength=self.config.l2_strength,
                                      bias=self.config.bias,
                                      use_batchnorm=self.config.use_batchnorm,
                                      is_training=self.is_training,
                                      name="score_feed2_upsample2x")

            all_feeds = tf.add(score_feed2_up, feed1_conv)

            print(all_feeds)

            # Upsample by 8x.
            self.logits = upsample(all_feeds,
                                   upsample_factor=8,
                                   output_shape=[self.input_shape[1], self.input_shape[2], self.config.num_classes],
                                   l2_strength=self.config.l2_strength,
                                   bias=self.config.bias,
                                   use_batchnorm=self.config.use_batchnorm,
                                   is_training=self.is_training,
                                   name="all_feeds_upsample8x")

            print(self.logits)
