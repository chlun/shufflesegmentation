import tensorflow as tf
from models.shufflenet import ShuffleNet
from models.skipnet import SkipNet
from models.base_model import BaseModel


class ShuffleSeg(BaseModel):
    def __init__(self, config, image_tensor, mask_tensor, class_weights=None):
        super(ShuffleSeg, self).__init__(config, image_tensor, mask_tensor, class_weights)

        self.encoder = None
        self.decoder = None

    def build_model(self):
        self.init_input()

        # Init encoder, decoder
        self.encoder = ShuffleNet(self.config, self.x, self.is_training)
        print("Encoder built successfully.")
        self.decoder = SkipNet(
            self.config, self.is_training, self.encoder.score, self.encoder.feed1, self.encoder.feed2, tf.shape(self.x))
        print("Decoder built successfully.")

        self.logits = self.decoder.logits

        self.init_train_op()

        self.init_output()

        # Init the checkpoint saver.
        self.init_saver()

        print("ShuffleSeg built successfully.")

    def load_pretrained_weights(self):
        restore_ops = list(self.encoder.load_pretrained_weights())

        return restore_ops
