from convolutions import *
from utils.utils import load_pickle
import tensorflow as tf


class ShuffleNet:
    MEAN = [103.94, 116.78, 123.68]
    NORMALIZER = 0.017

    def __init__(self, config, x, is_training):
        self.config = config

        # Input
        self.x = x
        self.y = None
        self.is_training = is_training

        # Train op
        self.train_op = None

        # Outputs and loss
        self.feed1 = None
        self.feed2 = None
        self.score = None

        self.l2_reg = tf.constant(0.0)
        self.logits = None
        self.loss = None
        self.predictions = None
        self.accuracy = None

        # Number of filters for each layers.
        self.conv1_num_filters = 24
        self.num_filters_for_stage = { 2: 240, 3: 480, 4: 960 }

        # Initiate the model.
        self.build_model()

    def build_model(self):
        with tf.variable_scope("ShuffleNet"):
            red, green, blue = tf.split(self.x, num_or_size_splits=3, axis=3)
            normalised_input = tf.concat([tf.subtract(blue, ShuffleNet.MEAN[0]) * ShuffleNet.NORMALIZER,
                                          tf.subtract(green, ShuffleNet.MEAN[1]) * ShuffleNet.NORMALIZER,
                                          tf.subtract(red, ShuffleNet.MEAN[2]) * ShuffleNet.NORMALIZER,], 3)

            kernel_shape = [3, 3, self.config.num_channels, self.conv1_num_filters]

            conv1 = conv_bias_activation(x=normalised_input,
                                         kernel_shape=kernel_shape,
                                         strides=[1, 2, 2, 1],
                                         padding='SAME',
                                         bias=self.config.bias,
                                         l2_strength=self.config.l2_strength,
                                         use_batchnorm=True,
                                         activation=tf.nn.relu,
                                         is_training=self.is_training,
                                         name='conv1')

            print(conv1)

            max_pool = tf.nn.max_pool(conv1,
                                      ksize=[1, 3, 3, 1],
                                      strides=[1, 2, 2, 1],
                                      padding='SAME',
                                      name='max_pool')

            print(max_pool)

            stage2 = self._create_stage(max_pool, 2, 3)
            print(stage2)
            stage3 = self._create_stage(stage2, 3, 7)
            print(stage3)
            stage4 = self._create_stage(stage3, 4, 3)
            print(stage4)

            """
            global_pool = tf.nn.avg_pool(stage4,
                                         ksize=[1, 7, 7, 1],
                                         strides=[1, 1, 1, 1],
                                         padding='VALID',
                                         name='global_pool')

            fc = conv_bias_activation(x=global_pool,
                                      kernel_shape=[1, 1, global_pool.shape[-1], self.num_classes],
                                      strides=[1, 1, 1, 1],
                                      padding='VALID',
                                      bias=self.bias,
                                      l2_strength=self.l2_strength,
                                      use_batchnorm=False,
                                      activation=None,
                                      is_training=self.is_training,
                                      name='fc')

            print(fc)
            
            self.logits = tf.layers.flatten(fc)
            print(self.logits)

            self.predictions = tf.argmax(self.logits, axis=-1, output_type=tf.int32)
            """

            self.score = conv_bias_activation(x=stage4,
                                              kernel_shape=[1, 1, stage4.shape[-1], self.config.num_classes],
                                              strides=[1, 1, 1, 1],
                                              padding='VALID',
                                              bias=self.config.bias,
                                              l2_strength=self.config.l2_strength,
                                              use_batchnorm=False,
                                              activation=None,
                                              is_training=self.is_training,
                                              name='score')

            self.feed1 = stage2
            self.feed2 = stage3

    def _create_stage(self, x, stage, num_repeats):
        num_filters = self.num_filters_for_stage[stage]
        stage_layer = self._shufflenet_unit(x,
                                            num_filters=num_filters,
                                            strides=[1, 2, 2, 1],
                                            join_method='concat',
                                            use_group_conv=(stage != 2),
                                            name='stage' + str(stage) + '_0')

        for i in range(1, num_repeats + 1):
            stage_layer = self._shufflenet_unit(stage_layer,
                                                num_filters=num_filters,
                                                strides=[1, 1, 1, 1],
                                                join_method='add',
                                                use_group_conv=True,
                                                name='stage' + str(stage) + '_' + str(i))

        return stage_layer

    def _shufflenet_unit(self, x, num_filters, strides, join_method, use_group_conv, name=None):

        if join_method == 'concat':
            num_gconv2_filters = num_filters - x.get_shape()[3].value
        else:
            num_gconv2_filters = num_filters

        num_gconv1_filters = num_gconv2_filters // 4

        with tf.variable_scope(name):
            shortcut = x

            if use_group_conv:
                gconv1 = grouped_conv(x,
                                      kernel_shape=[1, 1, x.shape[-1], num_gconv1_filters],
                                      strides=[1, 1, 1, 1],
                                      num_groups=self.config.num_groups,
                                      use_batchnorm=True,
                                      l2_strength=self.config.l2_strength,
                                      bias=self.config.bias,
                                      activation=tf.nn.relu,
                                      is_training=self.is_training,
                                      name='Gbottleneck')

                shuffled = shuffle_channels(gconv1, self.config.num_groups, 'channel_shuffle')
            else:
                gconv1 = conv_bias_activation(x=x,
                                              kernel_shape=[1, 1, x.shape[-1], num_gconv1_filters],
                                              strides=[1, 1, 1, 1],
                                              padding='VALID',
                                              bias=self.config.bias,
                                              l2_strength=self.config.l2_strength,
                                              use_batchnorm=True,
                                              activation=tf.nn.relu,
                                              is_training=self.is_training,
                                              name='bottleneck')

                shuffled = gconv1

            padded = tf.pad(shuffled, [[0, 0], [1, 1], [1, 1], [0, 0]], "CONSTANT")

            depthwise = depthwise_conv(padded,
                                       kernel_shape=[3, 3, padded.shape[-1], 1],
                                       strides=strides,
                                       bias=self.config.bias,
                                       l2_strength=self.config.l2_strength,
                                       use_batchnorm=True,
                                       activation=None,
                                       is_training=self.is_training,
                                       name='depthwise')

            print(depthwise)

            group_conv2 = grouped_conv(depthwise,
                                       kernel_shape=[1, 1, depthwise.shape[-1], num_gconv2_filters],
                                       strides=[1, 1, 1, 1],
                                       num_groups=self.config.num_groups,
                                       use_batchnorm=True,
                                       bias=self.config.bias,
                                       l2_strength=self.config.l2_strength,
                                       activation=None,
                                       is_training=self.is_training,
                                       name='Gconv1x1')

            if join_method == 'concat':
                shortcut_pooled = tf.nn.avg_pool(shortcut, ksize=[1, 3, 3, 1], strides=strides, padding='SAME', name='avg_pool3x3')
                combined = tf.concat([shortcut_pooled, group_conv2], axis=-1)
            else:
                shortcut_pooled = shortcut
                combined = shortcut_pooled + group_conv2

            return tf.nn.relu(combined)

    def load_pretrained_weights(self):
        print("Loading ImageNet pretrained weights...")
        with tf.variable_scope("load_pretrained_weights"):
            variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
            try:
                pretrained_var = load_pickle(self.config.pretrained_path)
                run_list = []
                count = 0
                for variable in variables:
                    for key, value in pretrained_var.items():
                        if key + ":" in variable.name:
                            run_list.append(tf.assign(variable, value))
                            print(key)
                            count += 1

                print("Added " + str(count) + " variables to assign op\n\n")
                return run_list
            except KeyboardInterrupt:
                print("No pretrained ImageNet weights exist. Skipping...\n\n")
