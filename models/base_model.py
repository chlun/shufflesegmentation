import tensorflow as tf
import os


class BaseModel:
    def __init__(self, config, image_tensor, mask_tensor, class_weights):
        """
        :param config: The configurations.
        :param image_tensor: A tensor of shape [batch, height, width, channels]. The input image to the model.
        :param mask_tensor:  A tensor of shape [batch, height, width, 1]. The mask where each pixel is an integer
            in {0, 1, ..., num_classes, ignore} where ignore is the id of the void/ignore class.
        :param class_weights: A list of ints with length equal to the number of classes.
        """
        self.config = config
        self.class_weights = class_weights

        self.saver = None
        self.best_saver = None
        self.cur_epoch_tensor = None
        self.increment_cur_epoch_tensor = None
        self.global_step_tensor = None
        self.current_best_metric = None

        self.x = image_tensor
        self.y = mask_tensor
        self.is_training = None

        self.loss = None
        self.logits = None
        self.predictions = None
        self.accuracy = None

        self.optimizer = None
        self.train_op = None

        # Init the global step.
        self.init_global_step()
        # Init the epoch counter.
        self.init_cur_epoch()

    def save(self, sess):
        """Saves the checkpoint in the path defined in the config.

        :param sess: The current Tensorflow session.
        """
        print("Saving model...")
        checkpoint_prefix = os.path.join(self.config.checkpoint_dir, "model")
        self.saver.save(sess, checkpoint_prefix, self.global_step_tensor)
        print("Model saved")

    def save_best(self, sess):
        """Saves the best checkpoint separately from the other checkpoints.

        :param sess: The current Tensorflow session.
        """
        print("Saving current best model...")
        checkpoint_prefix = os.path.join(self.config.best_checkpoint_dir, "model")
        self.best_saver.save(sess, checkpoint_prefix, self.global_step_tensor)
        print("Model saved")

    # Load the latest checkpoint from the experiment path defined in the config file.
    def load(self, sess):
        """Loads the latest checkpoint or pretrained weights or start fresh if neither are found.

        :param sess: The current Tensorflow session.
        """
        latest_checkpoint = tf.train.latest_checkpoint(self.config.checkpoint_dir)
        if latest_checkpoint:
            print("Loading model checkpoint {} ...\n".format(latest_checkpoint))
            self.saver.restore(sess, latest_checkpoint)
            print("Model loaded")
        else:
            print("No checkpoints found. Try load pretrained weights if exists.")
            sess.run(tf.global_variables_initializer())
            try:
                sess.run(self.load_pretrained_weights())
            except FileNotFoundError:
                print("No pretrained weights found.")

    def init_input(self):
        with tf.variable_scope("Inputs"):
            self.is_training = False if self.config.mode == "test" else tf.placeholder(tf.bool)

            self.x = tf.identity(self.x, name="x")

            if self.config.mode != "test":
                self.y = tf.to_int32(self.y, name="y")

    def init_train_op(self):
        if self.config.mode == "test":
            return

        # Define loss.
        with tf.variable_scope("Loss"):
            weights = tf.one_hot(self.y, self.config.num_classes, dtype=tf.float32) * self.class_weights
            weights = tf.reduce_sum(weights, axis=4)
            cross_entropy = tf.losses.sparse_softmax_cross_entropy(labels=self.y, logits=self.logits, weights=weights)

            reg_loss = tf.reduce_sum(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
            self.loss = tf.reduce_mean(cross_entropy) + reg_loss

        # Define train op.
        with tf.name_scope('train-operation'):
            extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(extra_update_ops):
                self.optimizer = tf.train.AdamOptimizer(self.config.learning_rate)
                self.train_op = self.optimizer.minimize(self.loss, self.global_step_tensor)

    def init_output(self):
        with tf.name_scope("Predictions"):
            self.predictions = tf.argmax(self.logits, axis=3, name="predictions", output_type=tf.int32)

        if self.config.mode != "test":
            with tf.variable_scope("Metrics"):
                predictions = tf.expand_dims(self.predictions, -1)
                self.accuracy = tf.reduce_mean(tf.cast(tf.equal(self.y, predictions), dtype=tf.float32))
                self.current_best_metric = tf.Variable(0.0, trainable=False, dtype=tf.float32, name="best_IOU")

    def init_cur_epoch(self):
        """Initialise a Tensorflow variable to use it as an epoch counter.
        """
        with tf.variable_scope('cur_epoch'):
            self.cur_epoch_tensor = tf.Variable(0, trainable=False, name='cur_epoch')
            self.increment_cur_epoch_tensor = tf.assign(self.cur_epoch_tensor, self.cur_epoch_tensor + 1)

    def init_global_step(self):
        """Initialise a Tensorflow variable to use it as a global step counter.
        """
        with tf.variable_scope('global_step'):
            self.global_step_tensor = tf.Variable(0, trainable=False, name='global_step')

    def init_saver(self):
        self.saver = tf.train.Saver(tf.global_variables(), max_to_keep=self.config.max_to_keep)
        self.best_saver = tf.train.Saver(tf.global_variables(), max_to_keep=1)

    def build_model(self):
        """Builds the entire model. A model must be built before training or making predictions.
        """
        raise NotImplementedError

    def load_pretrained_weights(self):
        raise NotImplementedError

    def update_best_metric(self, new_best):
        """Saves the current best metric achieved by the model so far.

        :param new_best: A float. The new value to save.
        """
        return tf.assign(self.current_best_metric, new_best)
