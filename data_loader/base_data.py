import numpy as np

class BaseData:

    def __init__(self, config):
        self.config = config
        self.dataset_size = 0
        self.val_dataset_size = 0

        self.train_dataset = None
        self.val_dataset = None
        self.train_iterator = None
        self.val_iterator = None
        self.handle = None
        self.next_elements = None

        self._prepare_datasets()

    def _prepare_datasets(self):
        """Initialise the iterator for the training and validation dataset and the handle to switch between them.
        """
        raise NotImplementedError

    def get_batch(self):
        """Gets the Tensorflow op to get the next batch of data from either the training set or the validation set
        depending on which handle was used.

        :return: A Tensorflow operation to get the next batch of features and labels from the dataset.
        """
        return self.next_elements

    def get_class_weights(self):
        """Returns the weight for each class for computing a weighted loss.

        :return: A numpy array of length equal to the number of classes.
        """
        return np.ones([self.config.num_classes])

    @classmethod
    def check_name(cls, loader_name):
        """Checks whether the supplied parameter is equal to the class's NAME member..

        :param loader_name: A string. The name to be checked against.
        """
        return loader_name == cls.NAME