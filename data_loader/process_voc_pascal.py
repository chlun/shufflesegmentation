import tensorflow as tf
import numpy as np
import os
import matplotlib.pyplot as plt
from PIL import Image
from tqdm import tqdm
from utils import dirs


# Default paths and filenames.
tfrecords_train_filename = "voc2012_segmentation_train.tfrecords"
tfrecords_val_filename = "voc2012_segmentation_val.tfrecords"
tfrecords_test_filename = "voc2012_segmentation_test.tfrecords"
tfrecords_dir = "./tfrecords"

imagesets_train = "./VOC2012/ImageSets/Segmentation/train.txt"
imagesets_val = "./VOC2012/ImageSets/Segmentation/val.txt"
img_dir = "./VOC2012/JPEGImages"
mask_dir = "./VOC2012/SegmentationClass"


# Number of classes including background and void.
num_classes = 22
# Default label of the void class.
ignore_label = 255


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _remap_mask_labels(mask, old_value, new_value):
    mask[mask == old_value] = new_value
    return mask


def write_to_tfrecords(filenames_path, img_dir, mask_dir, outfile, ignore_label=None, new_ignore_label=None):
    """Converts images and their corresponding segmentation masks to the TfRecord format.

    :param filenames_path: A string. Path to the file containing the filenames of the images and masks.
    :param img_dir: A string. The directory containing the images.
    :param mask_dir: A string. The directory containing the masks.
    :param outfile: A string. Path of the output file.
    :param ignore_label: An int. The label currently assigned to the void/ignore class.
    :param new_ignore_label: An int. The new label to assign to void/ignore class.
    """
    if ignore_label is not None and new_ignore_label is None:
        raise ValueError("Must provide an integer for new_ignore_label.")
    
    writer = tf.python_io.TFRecordWriter(outfile)
    
    with open(filenames_path, 'r', encoding='latin-1') as f:
        for line in tqdm(f.readlines()):
            line = line.replace("\n", "")
            img_filename = line + ".jpg"
            mask_filename = line + ".png"
            img_path = os.path.join(img_dir, img_filename)
            mask_path = os.path.join(mask_dir, mask_filename)

            img = np.array(Image.open(img_path))
            mask = np.array(Image.open(mask_path))

            if ignore_label is not None and new_ignore_label is not None:
                mask = _remap_mask_labels(mask, ignore_label, new_ignore_label)

            height = img.shape[0]
            width = img.shape[1]
            depth = img.shape[2]
            img_raw = img.tostring()
            mask_raw = mask.tostring()

            example = tf.train.Example(features=tf.train.Features(feature={
                'height': _int64_feature(height),
                'width': _int64_feature(width),
                'depth': _int64_feature(depth),
                'image_raw': _bytes_feature(img_raw),
                'mask_raw': _bytes_feature(mask_raw)}))

            writer.write(example.SerializeToString())

    writer.close()
    

def read_from_tfrecords(path):
    """Load and decode the TfRecord in the specified path.

    :param path: A string. Path to the TfRecord.
    :return: An array of image and mask tuples.
    """
    examples = []

    record_iterator = tf.python_io.tf_record_iterator(path=path)

    for string_record in record_iterator:
        example = tf.train.Example()
        example.ParseFromString(string_record)

        height = int(example.features.feature['height']
                                     .int64_list
                                     .value[0])

        width = int(example.features.feature['width']
                                    .int64_list
                                    .value[0])

        depth = int(example.features.feature['depth']
                                    .int64_list
                                    .value[0])

        img_string = (example.features.feature['image_raw']
                                      .bytes_list
                                      .value[0])

        mask_string = (example.features.feature['mask_raw']
                                    .bytes_list
                                    .value[0])

        img_flat = np.fromstring(img_string, dtype=np.uint8)
        img = img_flat.reshape((height, width, depth))

        mask_flat = np.fromstring(mask_string, dtype=np.uint8)

        # Masks don't have depth.
        mask = mask_flat.reshape((height, width))

        examples.append((img, mask))

    return examples


def save_as_tfrecords(train_filenames, val_filenames, out_dir, outfile_train, outfile_val):
    dirs.create_dirs([out_dir])

    write_to_tfrecords(train_filenames, img_dir, mask_dir, outfile_train, ignore_label, num_classes - 1)
    write_to_tfrecords(val_filenames, img_dir, mask_dir, outfile_val, ignore_label, num_classes - 1)


def load_from_tfrecords(tfrecords_train_path, tfrecords_val_path):
    dataset_train = read_from_tfrecords(tfrecords_train_path)
    dataset_val = read_from_tfrecords(tfrecords_val_path)

    return dataset_train, dataset_val


def filter_filenames(filenames_path, outfile, classes_to_keep):
    """From the given list of filenames, write to a new file the ones for which the image contains one or more
    of the specified classes.

    :param filenames_path: A string. Path to the file containing the list of filenames in the dataset.
    :param outfile: A string. Name of the output file.
    :param classes_to_keep: A list of ints representing the classes to keep.
    """

    with open(filenames_path, 'r', encoding='latin-1') as f:
        with open(outfile, 'w', encoding='latin-1') as fout:
            for line in tqdm(f.readlines()):
                line = line.replace("\n", "")
                mask_filename = line + ".png"
                mask_path = os.path.join(mask_dir, mask_filename)

                mask = np.array(Image.open(mask_path))

                if np.any(np.isin(mask, classes_to_keep)):
                    fout.write(line + '\n')


def main():
    # Inspect random files in the train and validation dataset.
    train_tfrecord = os.path.join(tfrecords_dir, tfrecords_train_filename)
    val_tfrecord = os.path.join(tfrecords_dir, tfrecords_val_filename)

    train, val = load_from_tfrecords(train_tfrecord, val_tfrecord)

    max_to_show = 10
    train_idx = np.random.permutation(len(train))
    val_idx = np.random.permutation(len(val))

    for idx in train_idx[: max_to_show]:
        img = train[idx][0]
        mask = train[idx][1]

        plt.imshow(img)
        plt.show()
        plt.imshow(mask)
        plt.show()

    for idx in val_idx[: max_to_show]:
        img = val[idx][0]
        mask = val[idx][1]

        plt.imshow(img)
        plt.show()
        plt.imshow(mask)
        plt.show()


if __name__ == '__main__':
    main()
