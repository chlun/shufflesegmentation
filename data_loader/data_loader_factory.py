from data_loader.voc_pascal import VocPascal


class DataLoaderFactory:
    # The list of registered derived classes of BaseData.
    DATA_LOADERS = [VocPascal]

    def create_loader(config):
        """Returns the data loader specified in the config file.

        :param config: The configurations.
        :return: A BaseData object.
        """
        for data_loader in DataLoaderFactory.DATA_LOADERS:
            if data_loader.check_name(config.data_loader):
                return data_loader(config)

    create_loader = staticmethod(create_loader)