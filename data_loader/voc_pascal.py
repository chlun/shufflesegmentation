from utils.image_utils import *
from data_loader.base_data import BaseData
import tensorflow as tf


class VocPascal(BaseData):
    NAME = "voc_pascal"

    # Weights for the loss function.
    class_weights = [1.8569058546990003, 37.83335014993935, 44.186816421869196, 35.57922035689313, 39.30355590693907, 39.28677433705933, 27.76620920758386, 30.513585390618736, 22.364948994806817, 32.85894219008357, 36.40144751433899, 30.907015972740098, 27.853393483220977, 35.33940348460588, 32.749377316284075, 15.714249448999276, 38.428197977866255, 35.47534875071824, 30.11561188672538, 28.92220053806821, 35.03906104990077, 0.0]

    def __init__(self, config):
        super(VocPascal, self).__init__(config)
        self.dataset_size = 1464
        self.val_dataset_size = 1449

    def _prepare_datasets(self):
        def _decode_image_and_mask(example_proto):
            features = {
                'height': tf.FixedLenFeature([], tf.int64),
                'width': tf.FixedLenFeature([], tf.int64),
                'depth': tf.FixedLenFeature([], tf.int64),
                'image_raw': tf.FixedLenFeature([], tf.string),
                'mask_raw': tf.FixedLenFeature([], tf.string)
            }

            parsed_features = tf.parse_single_example(example_proto, features)

            image = tf.decode_raw(parsed_features['image_raw'], tf.uint8)
            mask = tf.decode_raw(parsed_features['mask_raw'], tf.uint8)

            height = tf.cast(parsed_features['height'], tf.int32)
            width = tf.cast(parsed_features['width'], tf.int32)
            depth = tf.cast(parsed_features['depth'], tf.int32)

            image = tf.reshape(image, [height, width, depth])
            mask = tf.reshape(mask, [height, width, 1])

            target_height = self.config.img_height
            target_width = self.config.img_width

            if height != target_height or width != target_width:
                image, mask = resize_and_pad_image_and_label(image, mask, target_height, target_width)
                image = tf.reshape(image, [target_height, target_width, depth])
                mask = tf.reshape(mask, [target_height, target_width, 1])

            return image, mask

        def _decode_image_and_mask_with_augmentation(example_proto):
            image, mask = _decode_image_and_mask(example_proto)
            image, mask = randomly_crop_and_resize_image_and_label(image, mask, self.config.img_height, self.config.img_width)
            image, mask = randomly_flip_image_and_label_left_right(image, mask)

            return image, mask

        with tf.name_scope("data_import"):
            self.train_dataset = tf.data.TFRecordDataset(self.config.train_dataset_path)
            self.train_dataset = (self.train_dataset
                                  .shuffle(self.config.shuffle_buffer_size)
                                  .map(_decode_image_and_mask_with_augmentation, num_parallel_calls=self.config.num_parallel_calls)
                                  .batch(self.config.batch_size, drop_remainder=True)
                                  .prefetch(self.config.prefetch_size)
                                  .repeat())

            self.train_iterator = self.train_dataset.make_one_shot_iterator()

            self.val_dataset = tf.data.TFRecordDataset(self.config.val_dataset_path)
            self.val_dataset = (self.val_dataset
                                .shuffle(self.config.shuffle_buffer_size)
                                .map(_decode_image_and_mask, num_parallel_calls=self.config.num_parallel_calls)
                                .batch(self.config.batch_size, drop_remainder=True)
                                .prefetch(self.config.prefetch_size)
                                .repeat())

            self.val_iterator = self.val_dataset.make_one_shot_iterator()

            self.handle = tf.placeholder(tf.string, shape=[])

            iter = tf.data.Iterator.from_string_handle(
                self.handle, self.train_dataset.output_types, self.train_dataset.output_shapes)

            self.next_elements = iter.get_next()

    def get_class_weights(self):
        return VocPascal.class_weights
