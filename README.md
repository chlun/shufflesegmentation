#ShuffleSeg

This is my TensorFlow implementation of ShuffleSeg, a convolutional neural network for semantic segmentation.
It is of an encoder-decoder architecture with Shufflenet as the encoder and FCN8s as the decoder.

See the original paper https://arxiv.org/pdf/1803.03816.pdf for more details.

I have also created a camera app for Android that simulates the depth of field effect using this model.

You can download the app on F-Droid [here](https://f-droid.org/en/packages/com.lun.chin.aicamera/)

![get it on f-droid](https://bitbucket.org/chlun/aicamera/raw/master/images/get-it-on-small.png)

or download the APK directly [here](https://drive.google.com/drive/folders/1z-37dH770bPoWoIb1DD54MTXxLpB_TYI?usp=sharing).

##Results

The network has been trained on the Pascal VOC 2012 train dataset. 
The best checkpoint can be downloaded and the latest checkpoints can be downloaded [here](https://drive.google.com/drive/folders/1MybD-9js_bm27QK_8-0OOto8TJfi6V_2?usp=sharing).

![example1](https://bitbucket.org/chlun/shufflesegmentation/raw/master/images/img1.png)
![example1](https://bitbucket.org/chlun/shufflesegmentation/raw/master/images/img2.png)
![example1](https://bitbucket.org/chlun/shufflesegmentation/raw/master/images/img3.png)

##Dependencies
```
bunch
matplotlib
numpy
Pillow
sklearn
tdqm
tensorflow
```

##Usage

To train the model on your own dataset subclass data_loader and implement the necessary methods.
If you wish to use Pascal VOC 2012 then you can use the existing VocPascal class.
The class expects the dataset to be in the ```.tfrecords``` format. 
The already converted dataset can be downloaded [here](https://drive.google.com/drive/folders/1MybD-9js_bm27QK_8-0OOto8TJfi6V_2?usp=sharing).
Next, modify ```configs/shuffleseg_train.json``` as appropriate or use as is and just set the path to the location of the dataset.

###Run

To start training (modify path to the ```.json``` file as appropriate):  

```
python main.py -c ./configs/shuffleseg_train.json
```

###Freeze graph

You can export your trained model as a single ```.pb``` file to use for example in mobile app development.
Modify ```configs/export_shuffleseg.json``` appropriately and run

```
python export.py -c ./configs/export_shuffleseg.json
```

