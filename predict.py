import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from utils.image_utils import apply_mask
from utils.config import process_config
from utils.utils import get_args
from models.shuffleseg import ShuffleSeg


def predict(config):
    with tf.Graph().as_default() as graph:
        loaded = False
        try:
            with tf.gfile.GFile(config.output_graph_name, 'rb') as f:
                graph_def = tf.GraphDef()
                serialised_graph = f.read()
                graph_def.ParseFromString(serialised_graph)
                image_tensor, mask_tensor = tf.import_graph_def(
                    graph_def,
                    name='',
                    return_elements=[
                        config.input_tensor_name + ":0",
                        config.output_tensor_name + ":0"
                    ])
            loaded = True
        except Exception as ex:
            print("Exception thrown whilst loading frozen graph: {0}\n".format(repr(ex)))

        with tf.Session(graph=graph) as sess:
            if not loaded:
                image_tensor = tf.placeholder(tf.float32, [1, None, None, config.num_channels])
                model = ShuffleSeg(config, image_tensor=image_tensor, mask_tensor=None)
                model.build_model()
                model.load(sess)
                mask_tensor = model.predictions

            for img in config.images:
                img = np.array(Image.open(img), dtype=np.float32)
                expanded_img = np.expand_dims(img, 0)

                predictions = sess.run(mask_tensor, feed_dict={image_tensor: expanded_img})
                mask = predictions[0]

                masked_img = apply_mask(img.astype(np.int32), mask)

                plt.imshow(masked_img)
                plt.show()


def main():
    try:
        args = get_args()
        print("Loading config file {0}".format(args.config))
        config = process_config(args.config)
    except Exception as ex:
        print("Missing or invalid arguments: {0}".format(ex.args))
        exit(0)

    predict(config)


if __name__ == '__main__':
    main()
